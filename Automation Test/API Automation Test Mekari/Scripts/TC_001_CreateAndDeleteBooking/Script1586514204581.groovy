import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper as JsonSlurper

Response1 = WS.sendRequest(findTestObject('CreateToken'))

def jsonS = new JsonSlurper()

def jsonResponse1 = jsonS.parseText(Response1.responseText)

authToken = jsonResponse1.token

GlobalVariable.globalToken = authToken

println(Response1.getResponseText())

println(GlobalVariable.globalToken)

WS.verifyResponseStatusCode(Response1, 200)

WS.delay(1)

Response2 = WS.sendRequest(findTestObject('CreateBooking', [('fname') : 'Fiedely', ('lname') : 'Halim', ('price') : 747, ('deposit') : true
            , ('checkindate') : '2020-04-07', ('checkoutdate') : '2020-04-08', ('needs') : 'Late Checkin']))

def jsonResponse2 = jsonS.parseText(Response2.responseText)

bookID = jsonResponse2.bookingid

GlobalVariable.bookIDGlobal = bookID

println(Response2.getResponseText())

println(GlobalVariable.bookIDGlobal)

WS.verifyResponseStatusCode(Response2, 200)

WS.delay(1)

Response3 = WS.sendRequest(findTestObject('GetBooking'))

println(Response3.getResponseText())

WS.verifyResponseStatusCode(Response3, 200)

WS.delay(1)

Response4 = WS.sendRequest(findTestObject('DeleteBooking'))

println(Response4.getResponseText())

WS.verifyResponseStatusCode(Response4, 201)

WS.delay(1)

Response5 = WS.sendRequest(findTestObject('GetBooking'))

println(Response5.getResponseText())

WS.verifyResponseStatusCode(Response5, 404)

