<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>Create booking</description>
   <name>CreateBooking</name>
   <tag></tag>
   <elementGuidId>50959843-04de-4d5d-be89-d4e16539453e</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;firstname\&quot; : \&quot;${fname}\&quot;,\n    \&quot;lastname\&quot; : \&quot;${lname}\&quot;,\n    \&quot;totalprice\&quot; : ${price},\n    \&quot;depositpaid\&quot; : ${deposit},\n    \&quot;bookingdates\&quot; : {\n        \&quot;checkin\&quot; : \&quot;${checkindate}\&quot;,\n        \&quot;checkout\&quot; : \&quot;${checkoutdate}\&quot;\n    },\n    \&quot;additionalneeds\&quot; : \&quot;${needs}\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Accept</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://restful-booker.herokuapp.com/booking</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>'Fiedely'</defaultValue>
      <description></description>
      <id>57e90d35-260f-434c-aa63-552793fff71c</id>
      <masked>false</masked>
      <name>fname</name>
   </variables>
   <variables>
      <defaultValue>'Halim'</defaultValue>
      <description></description>
      <id>a2e7f5b5-1ea1-456e-911c-7ca4b8a33c65</id>
      <masked>false</masked>
      <name>lname</name>
   </variables>
   <variables>
      <defaultValue>777</defaultValue>
      <description></description>
      <id>c51f7a79-98a7-4d81-8747-280d4df95d71</id>
      <masked>false</masked>
      <name>price</name>
   </variables>
   <variables>
      <defaultValue>true</defaultValue>
      <description></description>
      <id>71f625c4-dc1d-443a-acac-9b7195bf9a25</id>
      <masked>false</masked>
      <name>deposit</name>
   </variables>
   <variables>
      <defaultValue>'2020-04-05'</defaultValue>
      <description></description>
      <id>3e4d1318-38cf-4299-9b9c-848067c9cb1a</id>
      <masked>false</masked>
      <name>checkindate</name>
   </variables>
   <variables>
      <defaultValue>'2020-04-07'</defaultValue>
      <description></description>
      <id>0a5952db-8b53-4dce-8147-61ce827a2a8f</id>
      <masked>false</masked>
      <name>checkoutdate</name>
   </variables>
   <variables>
      <defaultValue>'Late Checkin'</defaultValue>
      <description></description>
      <id>2bf84a29-0482-4599-94ff-66bdb7218a75</id>
      <masked>false</masked>
      <name>needs</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
