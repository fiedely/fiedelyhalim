<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_002_SignInSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>fe62b154-3143-4294-aac3-34dc24d08558</testSuiteGuid>
   <testCaseLink>
      <guid>ca408c1c-385e-4f8a-8c79-fd174ed3f94f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC_003_ExceptionSignInWrongPassword</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>728949ee-7ced-4d87-afb4-078301c7613a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC_004_ExceptionSignInWrongEmail</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
