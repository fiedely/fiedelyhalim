<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_001_SignUpSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>aaa75318-99a6-4351-873d-87acc40bcbe3</testSuiteGuid>
   <testCaseLink>
      <guid>805eedb2-1846-4c87-bdac-eb2b29487969</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC_001_ExceptionSignUpWithRegisteredEmail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42549b31-9488-46a6-bd55-bfe58b18fa96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC_002_ExceptionSignUpPasswordNotMatch</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
