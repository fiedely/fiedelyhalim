Create and Delete Booking Automation Guide :

1. Open Project "API Automation Test Mekari" with Katalon Studio

2. In Test Explorer, expand the Test Cases folder and open "TC_001_CreateAndDeleteBooking"

3. Click Run button to initiate the automation

4. Open Console view to see the automation log